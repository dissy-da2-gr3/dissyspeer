package transaction

import (
	"RSA"
	"fmt"
	"github.com/google/uuid"
	"testing"
)

func TestSigningAndVerification(t *testing.T) {
	myKeys := RSA.GenerateKeyPair(2048)

	myAccountID := RSA.EncodePublicKey(myKeys.Public)
	initialTransaction := SignedTransaction{ID: uuid.NewString(), From: myAccountID, To: "TestRecipient", Amount: 1337, Signature: ""}
	fmt.Printf("initial transaction ID: %s", initialTransaction.ID)
	SignTransaction(&initialTransaction, myKeys.Secret)

	if initialTransaction.Signature == "" {
		t.Errorf("Signing failed :(")
	}
	fmt.Printf("Signature of transaction with ID: %s is now: %s\n", initialTransaction.ID, initialTransaction.Signature)

	verification := VerifySignedTransaction(&initialTransaction)
	if !verification {
		t.Errorf("Failed to verify signature of message with ID: %s\n", initialTransaction.ID)
	} else {
		fmt.Printf("OK!\n")
	}
}

func TestMarshalling(t *testing.T) {

	initialTransaction := SignedTransaction{ID: uuid.NewString(), From: "TestSender", To: "TestRecipient", Amount: 1337, Signature: "NONE"}

	jsonData := Marshaler(&initialTransaction)

	// Check if the marshalled data matches the original struct
	unmarshalledMsg := UnMarshaler(jsonData)

	if unmarshalledMsg.ID != initialTransaction.ID || unmarshalledMsg.From != initialTransaction.From ||
		unmarshalledMsg.To != initialTransaction.To || unmarshalledMsg.Amount != initialTransaction.Amount || unmarshalledMsg.Signature != initialTransaction.Signature {
		t.Errorf("Unmarshalled message does not match the original message. Got %+v, want %+v", unmarshalledMsg, initialTransaction)
	}
	fmt.Println("Marshalled succesfully. ")
}
