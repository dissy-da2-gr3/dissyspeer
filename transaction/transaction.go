package transaction

import (
	"RSA"
	"crypto/sha256"
	"encoding/json"
	"log"
	"math/big"
)

/*
	type Transaction struct {
		ID     string
		From   string
		To     string
		Amount int
	}
*/
type SignedTransaction struct {
	ID        string `json:"id"`                  //Used as IV
	From      string `json:"from"`                //public key of sender (and signer)
	To        string `json:"to"`                  //public key of receiver
	Amount    int    `json:"amount"`              //Amount to transfer
	Signature string `json:"signature,omitempty"` //senders signature
}

func HashTransaction(t *SignedTransaction) *big.Int {
	tStripped := SignedTransaction{ //deep copy to new literal to avoid overwriting original.
		ID:     t.ID,
		From:   t.From,
		To:     t.To,
		Amount: t.Amount,
	}
	hashThisString := Marshaler(&tStripped) //get a string from the struct.
	hashbytes := sha256.New()               //Hash the thing to try and fit in a big.int (32 bytes)
	hashbytes.Write([]byte(hashThisString))
	m := new(big.Int)
	m.SetBytes(hashbytes.Sum(nil))
	return m
}
func SignTransaction(t *SignedTransaction, sk RSA.SecretKey) {
	m := HashTransaction(t)
	t.Signature = RSA.Signature(m, sk).Text(32)
}

func VerifySignedTransaction(tS *SignedTransaction) bool {

	pk := RSA.DecodePublicKey(tS.From) //Grab the public key from the transaction FROM field

	//The plan: Convert everything to big.int while conserving as much info as possible, so we can use the prwviously developed RSA lib.
	verification := HashTransaction(tS) //Strips the signature and hashes the rest of the transaction

	//parse the received signature to big.Int, so our RSA lib will eat it.
	original := new(big.Int)
	original.SetString(tS.Signature, 32)

	//MAGIC IS HERE:
	/*
		FROM should be the public key of the one who initiated the transaction,
		And signed it with their private key.
		So.. by verifying the signature we verify ownership of the account.
		(By verifying that the holder of the private key holds the account of the corresponding public key)
	*/
	return RSA.Verification(original, *pk, verification)

}
func Marshaler(transaction *SignedTransaction) string {
	//fmt.Printf("marshaling transaction: %+v", transaction)
	serialised, err := json.Marshal(&transaction)
	if err != nil {
		log.Panicf("Error marshaling signed transaction :/")
	}
	return string(serialised)
}

func UnMarshaler(incoming string) *SignedTransaction {
	t := SignedTransaction{}
	err := json.Unmarshal([]byte(incoming), &t)
	if err != nil {
		log.Panicf("Error unmarshaling signed transaction :/")
	}
	//fmt.Printf("unmarshaled transaction: %+v", t)
	return &t
}
