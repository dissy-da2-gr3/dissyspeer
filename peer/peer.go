package peer

import (
	"account"
	"bufio"
	"encoding/json"
	"fmt"
	"go/types"
	"log"
	"message"
	"net"
	"strconv"
	"strings"
	"sync"
	"transaction"
)

// quick types for ease of coding:
type MessageMap map[string]message.Message //Here's how we store a collection of messages: key->Message ID (from uuid.NewString())
type ConnMap map[string]net.Conn           //Here's how we locally store our list of known peers: Key->Address=concat(IP,":",PORT), Value=net.Conn
type PortablePeerMap map[string]types.Nil  //Here's how we unmarshal a list of peers.

// quick and dirty for the advanced flooding solution... to hold a log of recipients for each message id:
// ......................PEERS      MESSAGE IDs
//type mapOfRecipients map[string]map[string]types.Nil
//type MessageIDList map[string]map[string]bool //Key: Message ID val: List of peers that have seen the message

type FloodLog struct {
	Lock    sync.Mutex
	Flooded MessageMap
	//Recipients mapOfRecipients
}

// types for peer 'state':
type LocalPeerMap struct {
	Lock  sync.Mutex
	Peers ConnMap
}

type Peer struct {
	//mu   sync.Mutex
	MyAddr        string
	KnownPeerAddr string
	MyPeers       LocalPeerMap
	FloodLog      FloodLog
	Server        net.Listener
	MyLedger      *account.Ledger
}

// Make a new peer
func NewPeer() *Peer {
	return &Peer{
		MyPeers: LocalPeerMap{
			Lock:  sync.Mutex{},
			Peers: make(ConnMap),
		},
		FloodLog: FloodLog{
			Lock:    sync.Mutex{},
			Flooded: make(MessageMap),
			//Recipients: make(mapOfRecipients),
		},
		MyLedger: account.MakeLedger(),
	}
}

func (p *Peer) AddPeerToList(addr string, conn net.Conn) {
	////p.MyPeers.Lock.Lock() //Locking is done in caller scope now!
	////defer p.MyPeers.Lock.Unlock()
	//before := len(p.MyPeers.Peers)
	p.MyPeers.Peers[addr] = conn
	/*
		after := len(p.MyPeers.Peers)
		if before != after {
			fmt.Printf("%s:✔️ -> My peerlist now contains %d other peers! (was %d)\n", p.MyAddr, after, before)
		}
	*/
}

func (p *Peer) Connect(addr string, port int) {

	p.KnownPeerAddr = fmt.Sprintf("%s:%d", addr, port)
	p.StartServer()                               //start listening for calls
	conn, err := net.Dial("tcp", p.KnownPeerAddr) //Dialout!
	if err != nil {
		fmt.Printf("%s:⚠️ No one answered my first call.. :( Reason given: \"%s\" ... Just gonna start my own thing...🤷\n", p.MyAddr, err.Error())
		return
	}
	getPeersMSG := message.CreateMessage(message.GETPEERS, "", p.MyAddr) //Getpeer messages don't need payloads

	go p.handleConnection(conn)
	//Maybe add a tiny delay here, so the buffered reader in connection handler is ready? ...
	//Or maybe sending, receiving, parsing, responding to a GETPEERS message is slower than setting up the buffer? .. yeah, probably.
	go p.TransmitMessageToPeer(conn, getPeersMSG)

}

func (p *Peer) StartServer() {
	listener, err := net.Listen("tcp", ":")
	if err != nil {
		panic(err)
	}
	p.Server = listener
	p.MyAddr = listener.Addr().String()

	//fmt.Printf("%s:🚀 ALIVE and waiting for incoming\n", p.MyAddr)

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Panicf("%s:🚩 Error in connection: %s\n", p.MyAddr, err)
				return
			}
			go p.handleConnection(conn) //Spin up a thread for each incoming connection :)
		}
	}()
}

// We store peer addresses as concatenated strings in the form IP:PORT, this makes map lookup smoother, and ensures that peers can share an IP address
func (p *Peer) GetAddressAndPort() (string, int) {
	lastIndexOfColon := strings.LastIndex(p.MyAddr, ":")

	ip := p.MyAddr[0:lastIndexOfColon]
	port, _ := strconv.Atoi(p.MyAddr[lastIndexOfColon+1:])

	return ip, port
}

func (p *Peer) DoTransaction(doThisTransaction *transaction.SignedTransaction) {
	OK := p.MyLedger.SignedTransaction(doThisTransaction)
	if OK {
		p.FloodTransaction(doThisTransaction)
	}

}

func (p *Peer) FloodTransaction(tx *transaction.SignedTransaction) {
	payload := transaction.Marshaler(tx)
	transactionMsg := message.CreateMessage(message.TRANSACTION, payload, p.MyAddr)
	p.FloodMessage(transactionMsg)
}

func (p *Peer) FloodMessage(msg message.Message) {
	p.FloodLog.Lock.Lock()
	defer p.FloodLog.Lock.Unlock()

	_, OK := p.FloodLog.Flooded[msg.ID] //check if message was previously flooded

	if !OK { //If not sent before
		p.MyPeers.Lock.Lock()
		defer p.MyPeers.Lock.Unlock()
		for _, conn := range p.MyPeers.Peers {
			go p.TransmitMessageToPeer(conn, msg)
		}
		p.FloodLog.Flooded[msg.ID] = msg
	} else {
		fmt.Printf("%s: Message (ID: %s, TYPE: %d) not flooded, as it was previously sent.\n", p.MyAddr, msg.ID, msg.Type)
	}
}

func (p *Peer) TransmitMessageToPeer(conn net.Conn, msg message.Message) {
	serialisedMsg := message.Marshaler(msg, p.MyAddr)

	write, err := conn.Write(serialisedMsg)

	if err != nil {
		log.Panicf("%s:🚩 Error sending message to %s :(...%d -> %s\n", p.MyAddr, conn.RemoteAddr(), write, err.Error())
		return
	}

}

func (p *Peer) transmitPeerListToPeer(conn net.Conn) {

	payload := p.MarshalPeerList()

	msg := message.CreateMessage(message.PEERLIST, payload, p.MyAddr)
	//fmt.Printf("%s: About to send peerlist: %s to %s\n", p.MyAddr, payload, conn.RemoteAddr())
	p.TransmitMessageToPeer(conn, msg)
}

func (p *Peer) handleConnection(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)
	for {
		msg, err := reader.ReadBytes('\n')
		if err != nil {
			fmt.Errorf("%s: Huh...  a peer (connecting from %s) just hung up on me.. : %s\n", p.MyAddr, conn.RemoteAddr().String(), err)
			return
		} else {
			go p.parseMessage(msg, conn) //every new message gets its own thread.
		}
	}
}

func (p *Peer) parsePeerlistPayload(RX message.Message, conn net.Conn) ConnMap {
	incomingPlist := p.UnmarshalPeerList(RX.Payload)

	newPeers := make(ConnMap)

	for receivedPeer, _ := range incomingPlist { //run through received peers
		if receivedPeer == p.MyAddr {
			continue //Skip myself
		}
		if receivedPeer == RX.Origin {
			newPeers[RX.Origin] = conn
			continue //don't try and establish a connection to the peer who sent the message, we already have a socket, just add it to the list of peers
		}
		p.MyPeers.Lock.Lock()
		if _, ok := p.MyPeers.Peers[receivedPeer]; !ok { //If i don't already have a conn with this peer
			newconn, err := net.Dial("tcp", receivedPeer) //establish connection
			if err != nil {
				log.Panicf("%s:🚩 Couldn't connect to new peer (%s) in list from %s: %s\n", p.MyAddr, receivedPeer, RX.Origin, err)
				continue //SKIP to the next one in the list.
			}
			newPeers[receivedPeer] = newconn
			go p.handleConnection(newconn) //Spin up a thread to handle incoming data from the new peer
		}
		p.MyPeers.Lock.Unlock()
	}
	return newPeers
}

func (p *Peer) MarshalPeerList() string {
	p.MyPeers.Lock.Lock()
	defer p.MyPeers.Lock.Unlock()

	outbound := PortablePeerMap{}
	//fmt.Printf("%s: Adding myself to my outbound PortablePeerMap\n", p.MyAddr)
	outbound[p.MyAddr] = types.Nil{}

	for k, _ := range p.MyPeers.Peers {
		//fmt.Printf("%s: Adding %s to my outbound PortablePeerMap\n", p.MyAddr, k)
		outbound[k] = types.Nil{}
	}

	serialised, err := json.Marshal(outbound)
	if err != nil {
		log.Panicf("%s:🚩  --> Error serialising message, %e, %s\n", p.MyAddr, err, serialised)
	}
	return fmt.Sprintf("%s", serialised)
}

func (p *Peer) UnmarshalPeerList(msg string) PortablePeerMap {
	pplist := PortablePeerMap{}
	err := json.Unmarshal([]byte(msg), &pplist)
	if err != nil {
		log.Panicf("%s:🚩  --> Error deserialising peer list: %s -> %e\n", p.MyAddr, msg, err)
	}
	return pplist
}

func (p *Peer) parseMessage(rawMessage []byte, conn net.Conn) {

	RX := message.Unmarshaler(rawMessage, p.MyAddr)

	//fmt.Printf("%s#: Messaged with ID %s originating from: %s\n", p.MyAddr, RX.ID, RX.Origin)
	switch RX.Type {
	case message.GETPEERS:
		//fmt.Printf("%s: got GETPEERS msg from %s, (via its outgoing port: %s)\n", p.MyAddr, RX.Origin, conn.RemoteAddr())
		p.transmitPeerListToPeer(conn)

	case message.PEERLIST:
		//BEWARE!! THE MAGIC HERE IS THAT PEERS INIT CONNECTIONS FROM RANDOM PORTS, AND TELL OTHERS HOW TO CALL BACK.
		//IMPORTANT: A PEER IS NAMED, KNOWN, STORED WITH "IP:PORT" OF ITS LISTENING SERVER, AND NOT ITS OUTGOING PORT.
		//fmt.Printf("%s: Incoming Portable Peerlist: %s\n", p.MyAddr, incomingPlist)
		newPeers := p.parsePeerlistPayload(RX, conn)
		p.MyPeers.Lock.Lock()
		for k, v := range newPeers {
			p.AddPeerToList(k, v) //Add peers to local Connmap... This will cause a lot of lock/unlock if the incoming list is large...
		}
		p.MyPeers.Lock.Unlock()

		MyJoinMessage := message.CreateMessage(message.JOIN, "", p.MyAddr) // Join messages don't need payloades.
		p.FloodMessage(MyJoinMessage)

	case message.JOIN:
		//fmt.Printf("%s: Got join message from %s, adding them to my peerlist with key: %s\n", p.MyAddr, RX.Origin, RX.Origin)
		p.MyPeers.Lock.Lock() //Locking is done in caller scope now!
		p.AddPeerToList(RX.Origin, conn)
		p.MyPeers.Lock.Unlock()

	case message.TRANSACTION:
		//fmt.Printf("%s: RX: %s\n", p.MyAddr, RX.Payload)
		p.MyLedger.SignedTransaction(transaction.UnMarshaler(RX.Payload))

	default:
		fmt.Errorf("UNKNOWN MESSAGE TYPE!!")
	}
}
