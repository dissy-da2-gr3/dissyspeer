package peer

import (
	"RSA"
	"account"
	"fmt"
	"math/rand"
	"message"
	"net"
	"reflect"
	"sort"
	"testing"
	"time"
	"transaction"

	"github.com/google/uuid"
)

// Makes a new peer
func TestMakingNewPeer(t *testing.T) {
	peer := NewPeer()

	if peer == nil {
		t.Error("Expected peer to be initialized, but got nil")
	}

	if len(peer.MyPeers.Peers) != 0 {
		t.Errorf("Expected 0 peers, but got %d", len(peer.MyPeers.Peers))
	}
	fmt.Println("Made new peer. ")
}

// First peer starts own network

// TODO: Test that it listens
func TestFirstPeerMakesNewNetwork(t *testing.T) {
	peer := NewPeer()
	if peer.Server != nil {
		t.Errorf("Server not started")
	}
	fmt.Println("Started server")
}

// The other peer is added to the network
func TestAddPeerToList(t *testing.T) {
	peer := NewPeer()

	// Mock a net.Conn for test purposes
	conn, _ := net.Pipe()
	defer conn.Close()

	peer.AddPeerToList("test-address", conn)

	if _, exists := peer.MyPeers.Peers["test-address"]; !exists {
		t.Error("Expected 'test-address' to be present in the peer list, but it's not found")
	}
	fmt.Println("Added a new peer to peerlist. ")
}

// The message is of correct type

func TestFloodMessage(t *testing.T) {
	// Create two peers
	peer1 := NewPeer()
	peer2 := NewPeer()

	// Initialize servers for both peers
	peer1.StartServer()
	time.Sleep(time.Millisecond * 100) // Give some time for server to start
	peer2.StartServer()
	time.Sleep(time.Millisecond * 100)

	// Connect peer2 to peer1
	peer2.Connect(peer1.GetAddressAndPort())
	time.Sleep(time.Millisecond * 100)

	msg := message.Message{
		Origin: peer1.MyAddr,
		Type:   message.GETPEERS,
		ID:     uuid.NewString(),
	}

	peer1.FloodMessage(msg)
}

//TODO: This is basically the same as what we wanna do in handin.go... I'm gonna write the handin first, and then copy it to here...

var NumberOfPeers = 10                       //Number of peers to spin up in the flood network (maxes out at around 64 on my machine)
var NumberOfAccounts = 15                    //How many accounts will be SIMULTANEOUSLY flooding n transactions?
var NumerOfRandomTransactionsPerAccount = 10 //number of transactions per account
var peerlist = make([]Peer, NumberOfPeers)
var accountList = make([]RSA.Keys, NumberOfAccounts)

func TestPeerNetwork(t *testing.T) {
	//Make a list of peers for easier looping:
	fmt.Printf("\n\tNARRATOR: Please wait; RSA key generation takes a little while")
	fmt.Printf("\n\tNARRATOR: Constructing %d new peers, And generating keys for %d accounts.", NumberOfPeers, NumberOfAccounts)

	//peerlist = make([]peer.Peer, NumberOfPeers)
	for i := 0; i < NumberOfPeers; i++ {
		peerlist[i] = *NewPeer()

	}
	for i := 0; i < NumberOfAccounts; i++ {
		accountList[i] = *RSA.GenerateKeyPair(2048)

	}
	fmt.Printf(" Done.\n")
	fmt.Printf("\n\tNARRATOR: Spinning up peer #0 with a bogus address... It should time out after a short while and just wait for connections:\n\n")

	peerlist[0].Connect("nope", 0) //give it a dummy address that wont't work, so it will start its own thing.
	time.Sleep(100 * time.Millisecond)

	fmt.Printf("\n\tNARRATOR: Spinning up %d more peers: \n", NumberOfPeers-1)

	//Spin up the RNG that we use throughout the test for random peer and recipient selection:
	s := rand.NewSource(int64(time.Now().Nanosecond()))
	r := rand.New(s) // initialize local pseudorandom generator

	for i := 1; i < NumberOfPeers; i++ {
		randomIndex := r.Intn(i) //pick a random number from 0 to <i
		IP, randomPort := peerlist[randomIndex].GetAddressAndPort()
		fmt.Printf("\tNARRATOR: Asking peer #%d to connect to peer #%d (on port %d)\n", i, randomIndex, randomPort)
		go peerlist[i].Connect(IP, randomPort)
		time.Sleep(20 * time.Millisecond)

	}

	time.Sleep(50 * time.Millisecond)
	fmt.Printf("\n\tNARRATOR: Checking to see if every peerlist has the same length (%d):... ", NumberOfPeers-1)

	for i := 0; i < NumberOfPeers; i++ {
		if len(peerlist[i].MyPeers.Peers) != NumberOfPeers-1 {
			panic("NOPE!")
		}
	}
	fmt.Printf("Pass! ✔️\n\n")

	fmt.Printf("\tNARRATOR: Ledgers on all peers should be empty at the moment... ")

	for i := 0; i < NumberOfPeers; i++ {
		if len(peerlist[i].MyLedger.Accounts) != 0 {
			panic("NOPE!")
		}
	}
	fmt.Printf("Pass! ✔️\n\n")

	///OPEN ACCOUNTS:
	fmt.Printf("\tNARRATOR: Asking %d accounts to send themselves money. through randomly chosen peers.\n\tThereby announcing the existience of their addresses to the rest of the network.\n\t(These transactions will open their accounts.)\n\n", NumberOfAccounts)
	randomPeers := rand.Perm(NumberOfPeers) //use this to get a shuffled list of indexes for peers
	fmt.Printf("Randomly selected peers: ")

	for i := 0; i < NumberOfAccounts; i++ { //For each account, run a thread that spawn n transaction threads
		go func(accountIndex int) {
			encodedPubkey := RSA.EncodePublicKey(accountList[accountIndex].Public)
			fmt.Printf("%d, ", randomPeers[accountIndex%NumberOfPeers]) //use the modulo to support more accounts than there are peers. (avoids crash when i > len(peerlist))
			newTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: encodedPubkey, To: encodedPubkey, Amount: r.Intn(1337)}
			transaction.SignTransaction(&newTransaction, accountList[accountIndex].Secret)
			go peerlist[randomPeers[accountIndex%NumberOfPeers]].DoTransaction(&newTransaction) //Transact!
		}(i)
	}

	time.Sleep(1000 * time.Millisecond)

	fmt.Printf("\n\tNARRATOR: Checking that ledgers were updated (lenght>0) and that they match...\n")
	for i := 0; i < NumberOfPeers; i++ {
		if len(peerlist[i].MyLedger.Accounts) == 0 {
			t.Errorf("OH NO! Empty ledger on peer #%d", i)
		}
	}
	printAndCompareLedgers()

	///MOVE CURRENCY:
	fmt.Printf("\tNARRATOR: Lets move some virtual currency...\n")
	fmt.Printf("\tNARRATOR: Now we're simultaneously gonna do %d transactions from each of the %d accounts, at randomly chosen peers.\n", NumerOfRandomTransactionsPerAccount, NumberOfAccounts)
	fmt.Printf("\tAfther this, we compare the ledgers again.\n")
	fmt.Printf("\t(Check runs after a 1 second delay. Uses reflect.DeepEqual() to compare all ledgers to that of peer #0)\n")

	for i := 0; i < NumberOfAccounts; i++ { //For each account
		randomRecipients := rand.Perm(NumberOfAccounts) //use this to get a shuffled list of indexes for transaction recipients (TO: "")
		randomPeers = rand.Perm(NumberOfPeers)          //shuffle the peers

		for j := 0; j < NumerOfRandomTransactionsPerAccount; j++ {
			go func(accountindex int) {
				randomTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: RSA.EncodePublicKey(accountList[accountindex].Public), To: RSA.EncodePublicKey(accountList[randomRecipients[accountindex]].Public), Amount: r.Intn(1337)}
				transaction.SignTransaction(&randomTransaction, accountList[accountindex].Secret)
				go peerlist[randomPeers[accountindex%NumberOfPeers]].DoTransaction(&randomTransaction) //Transact!
			}(i)
		}

	}

	time.Sleep(1000 * time.Millisecond)
	printAndCompareLedgers()

	//Set up random TX and RX:
	randomSenderKeys := &accountList[r.Intn(len(accountList))]
	randomReceiverKeys := &accountList[r.Intn(len(accountList))]
	repeatingPeerIndex := r.Intn(len(peerlist))

	//Perform a transaction to mimic in replay attack:
	replayTestTransaction := &transaction.SignedTransaction{ID: uuid.NewString(), From: RSA.EncodePublicKey(randomSenderKeys.Public), To: RSA.EncodePublicKey(randomReceiverKeys.Public), Amount: r.Intn(1337)}
	transaction.SignTransaction(replayTestTransaction, randomSenderKeys.Secret)
	peerlist[repeatingPeerIndex].DoTransaction(replayTestTransaction) //Transact!

	time.Sleep(200 * time.Millisecond) //Let the transaction flood through the network

	//DEEPCOPY the ledger of a random peer for comparison after rejection tests
	ledgerBefore := make(map[string]int, NumberOfAccounts)
	for k, v := range peerlist[r.Intn(len(peerlist))].MyLedger.Accounts {
		ledgerBefore[k] = v
	}

	fmt.Printf("\tNARRATOR: Verifying correct rejection of:\n") //REPLAY ATTACKS HERE

	fmt.Printf("\tA) Local replay (peer.doTransaction):\n")           //LOCAL REPEAT
	peerlist[repeatingPeerIndex].DoTransaction(replayTestTransaction) //replaying from same peer -> it should never be broadccast!
	time.Sleep(200 * time.Millisecond)
	if !reflect.DeepEqual(ledgerBefore, peerlist[r.Intn(len(peerlist))].MyLedger.Accounts) {
		panic("Oh!.. Local replay attack wasn't rejected right :(")
	}
	fmt.Printf("\n\tB) Broadcast replay (peer.FloodTransaction):\n")
	//REBROADCAST FROM OTHER SOURCE:
	replayingPeerIndex := r.Intn(len(peerlist))
	peerlist[replayingPeerIndex].FloodTransaction(replayTestTransaction) //broadcasting from another peer -> it should be rejected by all recipients!
	time.Sleep(200 * time.Millisecond)
	if !reflect.DeepEqual(ledgerBefore, peerlist[r.Intn(len(peerlist))].MyLedger.Accounts) {
		panic("Oh!.. Broadcast replay wasn't rejected right :(")
	}

	fmt.Printf("\n\tC) Transactions with invalid signatures:\n")

	//Set up random TX and RX:
	randomSenderKeys = &accountList[r.Intn(len(accountList))]
	randomReceiverKeys = &accountList[r.Intn(len(accountList))]

	//simply not signed:
	unsignedT := &transaction.SignedTransaction{ID: uuid.NewString(), From: RSA.EncodePublicKey(randomSenderKeys.Public), To: RSA.EncodePublicKey(randomReceiverKeys.Public), Amount: r.Intn(1337)}

	fmt.Printf("\n\t0) Unsigned transaction:\n")
	peerlist[r.Intn(len(peerlist))].DoTransaction(unsignedT) //try to flood an unsigned transaction.
	time.Sleep(200 * time.Millisecond)
	if !reflect.DeepEqual(ledgerBefore, peerlist[r.Intn(len(peerlist))].MyLedger.Accounts) {
		panic("Oh!.. it looks like an unsigned transaction wasn't rejected right :(")
	}

	//Signature forgery:
	randomPerpetratorKeys := &accountList[r.Intn(len(accountList))]
	randomVictimKeys := &accountList[r.Intn(len(accountList))] //NOTICE THE VICTIM IN THE FROM field:
	forgedT := &transaction.SignedTransaction{ID: uuid.NewString(), From: RSA.EncodePublicKey(randomVictimKeys.Public), To: RSA.EncodePublicKey(randomPerpetratorKeys.Public), Amount: r.Intn(1337)}
	transaction.SignTransaction(forgedT, randomPerpetratorKeys.Secret)

	fmt.Printf("\n\t1: Locally, through peer.DoTransaction():\n")
	peerlist[r.Intn(len(peerlist))].DoTransaction(forgedT) //try to perform a forged transaction
	fmt.Printf("\n\t2: Everywhere else, through peer.FloodTransaction():\n")
	peerlist[r.Intn(len(peerlist))].FloodTransaction(forgedT) //try to flood a forged transaction

	time.Sleep(200 * time.Millisecond)
	if !reflect.DeepEqual(ledgerBefore, peerlist[r.Intn(len(peerlist))].MyLedger.Accounts) {
		panic("Oh!.. it looks like transactions with forged signatures weren't ignored correctly :(")
	}
	compareLedgers()

	/*
		fmt.Printf("\n\tD: Negative amounts:\n")
		randomPerpetratorKeys = &accountList[r.Intn(len(accountList))]
		randomVictimKeys = &accountList[r.Intn(len(accountList))] //NOTICE THE VICTIM IN THE FROM field:
		negativeTransaction := &transaction.SignedTransaction{ID: uuid.NewString(), From: RSA.EncodePublicKey(randomPerpetratorKeys.Public), To: RSA.EncodePublicKey(randomVictimKeys.Public), Amount: -100000}
		transaction.SignTransaction(negativeTransaction, randomPerpetratorKeys.Secret)
		peerlist[r.Intn(len(peerlist))].DoTransaction(negativeTransaction)

		time.Sleep(1500 * time.Millisecond)
		if !reflect.DeepEqual(ledgerBefore, peerlist[r.Intn(len(peerlist))].MyLedger.Accounts) {
			panic("WOW!.. it looks like transactions with negative amounts aren't ignored correctly... yet ...")
		}
		compareLedgers()
	*/

	fmt.Printf("\tThat's it. Thanks for playing...\n")

}

// Maps aren't sorted, so we do that to make it easier to visually compare (printout of) ledgers
func compareLedgers() {
	for i := 0; i < len(peerlist); i++ {
		for j := 0; j < len(peerlist); j++ {
			if !reflect.DeepEqual(peerlist[i].MyLedger.Accounts, peerlist[j].MyLedger.Accounts) {
				panic("Not all ledgers match after performing transactions!")
			}
		}
	}
	fmt.Printf("Ledgers Match! ✔️\n")

}
func printAndCompareLedgers() {
	fmt.Printf("\n\t(Showing ledgers with hashed account-names for brevity)\n\n")
	for i := 0; i < len(peerlist); i++ {
		fmt.Printf("\tLedger on Peer %d (holds %d accounts):\n", i, len(peerlist[i].MyLedger.Accounts))
		sortLedgerAndPrint(peerlist[i].MyLedger)
		fmt.Println()
		for j := 0; j < len(peerlist); j++ {
			if !reflect.DeepEqual(peerlist[i].MyLedger.Accounts, peerlist[j].MyLedger.Accounts) {
				panic("Not all ledgers match after performing transactions!")
			}
		}
	}
	fmt.Printf("Ledgers Match! ✔️\n")
}

func sortLedgerAndPrint(ledger *account.Ledger) {
	keys := make([]string, 0, len(ledger.Accounts))

	for k := range ledger.Accounts {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	sum := 0
	for _, k := range keys {
		fingerprint := RSA.HashBytes([]byte(k)) //Print the hashes of the keys to save terminal real-estate
		fmt.Printf("\t\t%x: %d\n", fingerprint, ledger.Accounts[k])
		sum += ledger.Accounts[k]
	}
	fmt.Printf("\t\tLedger balance: %d", sum)
	fmt.Printf(" ✔️\n")

}
