package account

import (
	"RSA"
	"fmt"
	"github.com/google/uuid"
	"reflect"
	"testing"
	"transaction"
)

func TestSigningAndVerification(t *testing.T) {
	myKeys := RSA.GenerateKeyPair(2048)
	testLedger := MakeLedger()
	myAccountID := RSA.EncodePublicKey(myKeys.Public)
	initialTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: myAccountID, To: "TestRecipient", Amount: 1337, Signature: ""}
	fmt.Printf("initial transaction ID: %s", initialTransaction.ID)
	transaction.SignTransaction(&initialTransaction, myKeys.Secret)
	testLedger.SignedTransaction(&initialTransaction)

	if initialTransaction.Signature == "" {
		t.Errorf("Signing failed :(")
	}
	fmt.Printf("Signature of transaction with ID: %s is now: %s\n", initialTransaction.ID, initialTransaction.Signature)

	verification := transaction.VerifySignedTransaction(&initialTransaction)
	if !verification {
		t.Errorf("Failed to verify signature of message with ID: %s\n", initialTransaction.ID)
	} else {
		fmt.Printf("OK!\n")
	}
}

func TestCorrectRejectionOfForgedSignatures(t *testing.T) {
	myKeys := RSA.GenerateKeyPair(2048)

	theirKeys := RSA.GenerateKeyPair(2048)
	//First we check that two ledgers can verify each other's signatures:
	firstLedger := MakeLedger()
	secondLedger := MakeLedger()

	firstID := RSA.EncodePublicKey(myKeys.Public)
	secondID := RSA.EncodePublicKey(theirKeys.Public)

	testTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: firstID, To: secondID, Amount: 1337}
	transaction.SignTransaction(&testTransaction, myKeys.Secret)

	firstLedger.SignedTransaction(&testTransaction)
	secondLedger.SignedTransaction(&testTransaction)

	//verify:
	expectedLedger := Ledger{Accounts: map[string]int{firstID: -1337, secondID: 1337}}

	firstAsExpected := reflect.DeepEqual(firstLedger.Accounts, expectedLedger.Accounts)
	secondAsExpected := reflect.DeepEqual(secondLedger.Accounts, expectedLedger.Accounts)
	if !(firstAsExpected && secondAsExpected) {
		t.Errorf("Ledger accounts not as expected after transaction.\nWant: %+v\nFirst: %+v\nSecond: %+v\n", expectedLedger.Accounts, firstLedger.Accounts, secondLedger.Accounts)
	}

	//Then we check that you can't sign on behalf of someone else.. by trying to steal some money...
	badTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: secondID, To: firstID, Amount: 1337}
	transaction.SignTransaction(&badTransaction, myKeys.Secret)
	firstLedger.SignedTransaction(&badTransaction)

	//expect no change
	firstAsExpected = reflect.DeepEqual(firstLedger.Accounts, expectedLedger.Accounts)
	secondAsExpected = reflect.DeepEqual(secondLedger.Accounts, expectedLedger.Accounts)
	if !(firstAsExpected && secondAsExpected) {
		t.Errorf("bad signature not rejected correcly!\nWant: %+v\nFirst: %+v\nSecond: %+v\n", expectedLedger.Accounts, firstLedger.Accounts, secondLedger.Accounts)
	}

}

func TestAccountGeneration(t *testing.T) {
	myKeys := RSA.GenerateKeyPair(2048)
	testLedger := MakeLedger()
	myAccountID := RSA.EncodePublicKey(myKeys.Public)

	//To broadcast your public key, and thus, open an account; Transfer any amount to yourself:
	testTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: myAccountID, To: myAccountID, Amount: 1337}
	transaction.SignTransaction(&testTransaction, myKeys.Secret)
	marshaled := transaction.Marshaler(&testTransaction)
	unmarshaled := transaction.UnMarshaler(marshaled)
	testLedger.SignedTransaction(unmarshaled)

	//verify:
	expectedLedger := Ledger{
		Accounts: map[string]int{myAccountID: 0},
	}
	if !reflect.DeepEqual(testLedger.Accounts, expectedLedger.Accounts) {
		t.Errorf("Ledger accounts not as expected after transaction. Got %+v, want %+v\n", testLedger.Accounts, expectedLedger.Accounts)
	}
}
func TestLedger(t *testing.T) {
	myKeys := RSA.GenerateKeyPair(2048)

	testLedger := MakeLedger()
	for k, _ := range testLedger.Accounts {
		_, OK := testLedger.Accounts[k]
		if OK {
			t.Errorf("Newly instantiated ledger isn't empty?!")
		}
	}
	myAccountID := RSA.EncodePublicKey(myKeys.Public)

	testTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: myAccountID, To: "TestRecipient", Amount: 1337}
	transaction.SignTransaction(&testTransaction, myKeys.Secret)

	marshaledTransaction := transaction.Marshaler(&testTransaction)
	// Check if the marshalled data matches the original struct
	unmarshaledTransaction := *transaction.UnMarshaler(marshaledTransaction)
	if !reflect.DeepEqual(testTransaction, unmarshaledTransaction) {
		t.Errorf("Unmarshalled transaction does not match the original. Got %+v, want %+v\n", unmarshaledTransaction, testTransaction)
	}

	//Do a transaction with the unmarshaled transaction just to make sure it still parses.
	testLedger.SignedTransaction(&unmarshaledTransaction)

	//verify:
	expectedLedger := Ledger{
		Accounts: map[string]int{myAccountID: -1337,
			"TestRecipient": 1337},
	}
	if !reflect.DeepEqual(testLedger.Accounts, expectedLedger.Accounts) {
		t.Errorf("Ledger accounts not as expected after transaction. Got %+v, want %+v\n", testLedger.Accounts, expectedLedger.Accounts)
	}

	fmt.Printf("Testing correct rejection of transactions:\n")
	//TODONE: Test correct rejection of repeated IDs.
	testLedger.SignedTransaction(&testTransaction)

	//TODONE: Test correct rejection of invalid signature:
	//TODO: Modify each field individually, to verify that they all contribute to the signature hash.
	testTransaction.ID = uuid.NewString() //we modify the UUID to circumvent the duplicate-UUID-countermeasure:
	//fmt.Printf("Current signature: %s\n", testTransaction.Signature)
	testLedger.SignedTransaction(&testTransaction) //should be rejected

	//Let's try and modify a signed transaction that hasn't been seen before:
	brandNewTransaction := transaction.SignedTransaction{ID: uuid.NewString(), From: myAccountID, To: "anyone...", Amount: 1337}
	transaction.SignTransaction(&brandNewTransaction, myKeys.Secret) //sign it
	brandNewTransaction.Amount = -500                                //modify the amount
	testLedger.SignedTransaction(&brandNewTransaction)               //test it - Should reject
	brandNewTransaction.Amount = 1337                                //restore amount
	testLedger.SignedTransaction(&brandNewTransaction)               //test it - Should accept

	expectedLedger.Accounts[myAccountID] -= 1337 //modify our expectations as well ....
	expectedLedger.Accounts["anyone..."] = 1337

	if !reflect.DeepEqual(testLedger.Accounts, expectedLedger.Accounts) {
		t.Errorf("Ledger accounts not as expected after rejection test. Got %+v, want %+v\n", testLedger.Accounts, expectedLedger.Accounts)
	}

	//TODONT: Want tests for robustness.. things go bad if transactions are malformed... like, e.g. a missing pipe seperator ('|') in the public key in the "To" field results in nastyness..
	//malFormedTestTransaction := SignedTransaction{ID: uuid.NewString(), From: "nope", To: "TestRecipient", Amount: 1337}
	//testLedger.SignTransaction(&malFormedTestTransaction)   //sign it
	//testLedger.SignedTransaction(&malFormedTestTransaction) //test it - Should reject

}
