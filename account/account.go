package account

import (
	"fmt"
	"sync"
	"transaction"
)

type Ledger struct {
	Accounts           map[string]int
	TransactionHistory map[string]int
	lock               sync.Mutex
}

func MakeLedger() *Ledger {
	ledger := new(Ledger)
	ledger.Accounts = make(map[string]int)
	ledger.TransactionHistory = make(map[string]int) //TODONE: Store IDs
	return ledger
}

func (l *Ledger) SignedTransaction(t *transaction.SignedTransaction) bool {
	l.lock.Lock()
	defer l.lock.Unlock()

	_, OK := l.TransactionHistory[t.ID]
	if OK {
		fmt.Printf("TRANSACTION (ID: %s) IGNORED; ALREADY PERFORMED!\n", t.ID)
		return false
	}

	//	TODONE: verify t.Signature validity here
	//validSignature := transaction.VerifySignedTransaction(t)
	validSignature := transaction.VerifySignedTransaction(t)
	if validSignature {
		l.Accounts[t.From] -= t.Amount
		l.Accounts[t.To] += t.Amount
		l.TransactionHistory[t.ID] = t.Amount //store the amount for some reason.. :)
		return true
	} else {
		fmt.Printf("TRANSACTION (ID: %s) IGNORED; Signature verification failed!\n", t.ID)
		return false
	}
}
