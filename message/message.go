package message

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"log"
)

type MsgType int

const (
	GETPEERS MsgType = iota
	JOIN
	TRANSACTION
	PEERLIST
)

type Message struct {
	Origin  string  `json:"origin"`
	Type    MsgType `json:"type"` //defined by the enums above
	ID      string  `json:"id"`   //Randomly generated UUID Using: uuid.NewString() - should be collision-proof?
	Payload string  `json:"payload"`
}

func CreateMessage(t MsgType, payload string, MyAddr string) (m Message) {
	switch t {
	case GETPEERS:
		m = Message{
			Origin: MyAddr,
			Type:   GETPEERS,
			ID:     uuid.NewString(),
		}
	case PEERLIST:
		m = Message{
			Origin:  MyAddr,
			Type:    PEERLIST,
			ID:      uuid.NewString(), //New UUID each call, so okay to bypass the check for if message has been sent before.
			Payload: payload,
		}
	case JOIN:
		m = Message{
			Origin: MyAddr,
			Type:   JOIN,
			ID:     uuid.NewString()}

	case TRANSACTION:
		m = Message{
			Origin:  MyAddr,
			Type:    TRANSACTION,
			ID:      uuid.NewString(),
			Payload: payload,
		}
	default:
		fmt.Errorf("UNKNOWN MESSAGE TYPE!!")
	}
	return m
}

// If message.Type is 'PEERLIST' its payload is unmarshaled seperately in p.ParseMessage
func Marshaler(msg Message, MyAddr string) []byte {
	serialised, err := json.Marshal(msg)
	if err != nil {
		log.Panicf("%s:🚩 --> Error serialising message, %e, %s\n", MyAddr, err, serialised)
	}

	//Check for newline termination and add if not found
	if serialised[len(serialised)-1] != '\n' {
		serialised = append(serialised, '\n')
	}
	//fmt.Printf("%s#: --> MESSAGE after serialisation: %s\n", p.MyAddr, string(serialised))
	return serialised
}

func Unmarshaler(serialised []byte, MyAddr string) Message {
	msg := Message{}
	//fmt.Printf("P%s: Message before deserialisation: %s\n", p.MyAddr, string(serialised))
	err := json.Unmarshal(serialised, &msg)
	if err != nil {
		log.Panicf("%s:🚩 --> Error deserialising message: %s , %e\n", MyAddr, serialised, err)
	}
	return msg
}
