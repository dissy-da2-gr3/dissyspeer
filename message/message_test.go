package message

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
)

//todo: Move the utility methods to message.go and test them individually

func TestMarshallingWithoutPayload(t *testing.T) {

	msg := CreateMessage(GETPEERS, "", "Nope:0")

	jsonData := Marshaler(msg, "Nope:0")

	// Check if the marshalled data matches the original struct
	var unmarshalledMsg Message
	if err := json.NewDecoder(bytes.NewReader(jsonData)).Decode(&unmarshalledMsg); err != nil {
		t.Fatalf("Failed to unmarshal the JSON data: %v", err)
	}

	if unmarshalledMsg.Origin != msg.Origin || unmarshalledMsg.Type != msg.Type ||
		unmarshalledMsg.ID != msg.ID || unmarshalledMsg.Payload != msg.Payload {
		t.Errorf("Unmarshalled message does not match the original message. Got %+v, want %+v", unmarshalledMsg, msg)
	}
	fmt.Println("Marshalled succesfully. ")
}
